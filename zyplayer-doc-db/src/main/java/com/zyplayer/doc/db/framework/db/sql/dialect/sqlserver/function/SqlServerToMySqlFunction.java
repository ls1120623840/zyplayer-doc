package com.zyplayer.doc.db.framework.db.sql.dialect.sqlserver.function;

import com.zyplayer.doc.db.framework.db.sql.MethodInvoke;

/**
 * sqlserver2mysql需要转换的函数
 *
 * @author diantu
 * @since 2023年2月2日
 */
public interface SqlServerToMySqlFunction extends MethodInvoke{

}
